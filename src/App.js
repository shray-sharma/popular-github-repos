import React from 'react';
import './App.scss';

import NavComponent from './components/Nav/nav';
import RepoList from './components/RepoList/repoList';
import Loading from './components/Loading/loading';

const languages = ['all', 'javascript', 'typescript', 'java', 'python', 'ruby', 'c', 'go'];

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      repos: [],
      language: 'all',
      loading: true,
    };

    this.handleSelectLanguage = this.handleSelectLanguage.bind(this);
    this.fetchPopularRepos = this.fetchPopularRepos.bind(this);
  }

  componentDidMount() {
    this.fetchPopularRepos(this.state.language);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.language !== this.state.language) {
      this.fetchPopularRepos(this.state.language);
    }
  }

  handleSelectLanguage(lang) {
    this.setState({
      language: lang,
    });
  }

  fetchPopularRepos(language) {
    // "language" can be "javascript", "ruby", "python", or "all"
    const encodedURI = encodeURI(
      `https://api.github.com/search/repositories?q=stars:>1+language:${language}&sort=stars&order=desc&type=Repositories`
    );

    this.setState({
      loading: true,
    });

    return fetch(encodedURI)
      .then(data => data.json())
      .then(repos => repos.items)
      .then(data => {
        this.setState({
          loading: false,
          repos: data,
        });
      })
      .catch(error => {
        console.warn(error);
        return null;
      });
  }

  render() {
    return (
      <div className="App">
        <div className="header">Popular GitHub Repositories</div>
        <NavComponent
          languages={languages}
          handleSelectLanguage={this.handleSelectLanguage}
        />
        {this.state.loading ? (
          <Loading />
        ) : (
          <RepoList repos={this.state.repos} />
        )}
      </div>
    );
  }
}

export default App;
