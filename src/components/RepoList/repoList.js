import React, { Component } from 'react';
import CardColumns from 'react-bootstrap/CardColumns';

import './repoList.scss';
import Repo from '../Repo/repo';

export class RepoList extends Component {
  render() {
    return (
      <CardColumns className="repo-list">
        {this.props.repos.map(repo => (
          <Repo key={repo.id} repo={repo} className="repo-list-item"/>
        ))}
      </CardColumns>
    );
  }
}

export default RepoList;
