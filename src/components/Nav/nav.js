import React, { Component } from 'react';
import { Nav } from 'react-bootstrap';

import './nav.scss';

export class NavComponent extends Component {
  render() {
    return (
      <div className="container">
        <Nav className="nav">
          {this.props.languages.map(lang => (
            <Nav.Item key={lang} className="nav-item" onClick={() => this.props.handleSelectLanguage(lang)}>
              <Nav.Link className="nav-link" eventkey="{lang}">
                {lang}
              </Nav.Link>
            </Nav.Item>
          ))}
        </Nav>
      </div>
    );
  }
}

export default NavComponent;
