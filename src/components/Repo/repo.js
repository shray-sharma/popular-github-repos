import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';

import { faStar, faCodeBranch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './repo.scss';

export class Repo extends Component {
  render() {
    return (
      <Card className="repo-card" bg="light">
        <Card.Body className="repo-card-body">
          <Card.Title className="repo-card-title">
            <a href={this.props.repo.owner.url}>
              <img
                src={this.props.repo.owner.avatar_url}
                className="repo-avatar"
                alt="@vinta"
              />
            </a>
            <a href={this.props.repo.html_url} className="repo-name">
              {this.props.repo.name}
            </a>
          </Card.Title>
          <Card.Text className="repo-card-description">
            {this.props.repo.description}
          </Card.Text>
          <Card.Text className="repo-stats">
            <FontAwesomeIcon icon={faStar} /> {this.props.repo.stargazers_count} &nbsp;&nbsp;
            <FontAwesomeIcon icon={faCodeBranch} /> {this.props.repo.forks_count}
          </Card.Text>
          <Card.Text className="text-muted">{this.props.repo.language}</Card.Text>
        </Card.Body>
      </Card>
    );
  }
}

export default Repo;
